package graphing.calculator



import org.junit.*
import grails.test.mixin.*

@TestFor(GraphController)
@Mock(Graph)
class GraphControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/graph/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.graphInstanceList.size() == 0
        assert model.graphInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.graphInstance != null
    }

    void testSave() {
        controller.save()

        assert model.graphInstance != null
        assert view == '/graph/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/graph/show/1'
        assert controller.flash.message != null
        assert Graph.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/graph/list'

        populateValidParams(params)
        def graph = new Graph(params)

        assert graph.save() != null

        params.id = graph.id

        def model = controller.show()

        assert model.graphInstance == graph
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/graph/list'

        populateValidParams(params)
        def graph = new Graph(params)

        assert graph.save() != null

        params.id = graph.id

        def model = controller.edit()

        assert model.graphInstance == graph
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/graph/list'

        response.reset()

        populateValidParams(params)
        def graph = new Graph(params)

        assert graph.save() != null

        // test invalid parameters in update
        params.id = graph.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/graph/edit"
        assert model.graphInstance != null

        graph.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/graph/show/$graph.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        graph.clearErrors()

        populateValidParams(params)
        params.id = graph.id
        params.version = -1
        controller.update()

        assert view == "/graph/edit"
        assert model.graphInstance != null
        assert model.graphInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/graph/list'

        response.reset()

        populateValidParams(params)
        def graph = new Graph(params)

        assert graph.save() != null
        assert Graph.count() == 1

        params.id = graph.id

        controller.delete()

        assert Graph.count() == 0
        assert Graph.get(graph.id) == null
        assert response.redirectedUrl == '/graph/list'
    }
}
