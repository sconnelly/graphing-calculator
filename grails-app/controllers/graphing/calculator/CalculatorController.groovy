package graphing.calculator

import de.congrace.exp4j.Calculable
import de.congrace.exp4j.ExpressionBuilder

class CalculatorController {
    static final DEFAULT_GRAPH_HEIGHT = 300
    static final DEFAULT_GRAPH_WIDTH = 500
    static final DEFAULT_GRAPH_LOWER_HORIZONTAL_BOUND = -5
    static final DEFAULT_GRAPH_UPPER_HORIZONTAL_BOUND = 5
    static final DEFAULT_GRAPH_LOWER_VERT_BOUND = -5
    static final DEFAULT_GRAPH_UPPER_VERT_BOUND = 5
    static final DEFAULT_GRAPH_TITLE = "Function Graph"
    static final DEFAULT_LOWER_BOUND = -5.0
    static final DEFAULT_UPPER_BOUND = 5.0
    static final DEFAULT_STEP = 0.1

    def index() {
        redirect(action: "createAndDisplay")
    }

    def createAndDisplay() {
        if (params.selectedGraphId) {
            params.id = params.selectedGraphId
        }
    }

    def save() {
        def functionInstance = new Function(params).save(failOnError: true)

        def graphInstance = new Graph(
                height: DEFAULT_GRAPH_HEIGHT,
                width: DEFAULT_GRAPH_WIDTH,
                horizontalLowerBound: DEFAULT_GRAPH_LOWER_HORIZONTAL_BOUND,
                horizontalUpperBound: DEFAULT_GRAPH_UPPER_HORIZONTAL_BOUND,
                verticalLowerBound: DEFAULT_GRAPH_LOWER_VERT_BOUND,
                verticalUpperBound: DEFAULT_GRAPH_UPPER_VERT_BOUND,
                title: DEFAULT_GRAPH_TITLE,
                functions: new HashSet([functionInstance])
        ).save(failOnError: true)

        redirect(action: "createAndDisplay", id: graphInstance.id)
    }

    /**
     * Creates a graph from a stored instance of Graph and its stored instances of Function
     * @param id
     * @return
     */
    def graphStoredData(Long id) {
        def graphInstance = Graph.get(id)

        if (!graphInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "index")
            return
        }

        def graphData = [] //data points to be used in drawing the graph
        def functions = graphInstance.getFunctions()

        //sort the functions by id for the sake of consistency
        functions.sort(new Comparator<Function>() {
            @Override
            int compare(Function o1, Function o2) {
                return o1?.id?.compareTo(o2?.id)
            }
        })

        def graphDataColumns = [['number', 'x']] //types and labels for axes
        def allResults = new ArrayList<HashMap>() //store the results for each function's calculations here

        functions?.each() { function ->
            ExpressionBuilder expressionBuilder = null //exp4j API
            Calculable calc = null //exp4j API
            def results = new HashMap() //the results for this function's calculations are stored here

            graphDataColumns.add(['number', 'f(x)=' + function?.expression]) //add the type and label for each function
            def expression = function?.expression //the mathematical function to evaluate

            //evaluate the function on the stored interval
            def lowerBound = function?.xLowerBound ?: DEFAULT_LOWER_BOUND
            def upperBound = function?.xUpperBound ?: DEFAULT_UPPER_BOUND

            //iterate over the points on the x-Axis
            for(def varX = lowerBound; varX <= upperBound; varX += DEFAULT_STEP) {
                //build up an expression using the exp4j API
                expressionBuilder = new ExpressionBuilder(expression)

                if(expression.indexOf("x") > -1) {
                    //if no value for x is specified, then the value of x is the current point along the x-Axis
                    expressionBuilder = expressionBuilder.withVariable("x", function?.xValue ?: varX)
                }

                if(expression.indexOf("y") > -1) {
                    //if there is no value defined for y, just set it to 0
                    expressionBuilder = expressionBuilder.withVariable("y", function?.yValue ?: 0)
                }

                calc = expressionBuilder.build()
                def result = calc.calculate()

                results.put(varX, result) //these results will be used to build up the data points
            }

            allResults.add(results)
        }

        //iterate over the results for each function and add the data points to graphData
        //using TreeSet will sort the keys, change it to HashSet and see what happens!
        def keySet = new TreeSet(allResults.get(0)?.keySet())

        //each key represents a point on the x-Axis
        keySet.each() { key ->
            def finalResults = [] //we will build this up as [x, result 1, result2, ...result n]
            finalResults.add(key) //the point on the x-Axis

            for(def i=0; i<allResults.size(); i++) {
                def results = allResults.get(i)
                finalResults.add(results.get(key)) //the result of the calculation for each function
            }

            graphData.add(finalResults)
        }

        //add the following to the model; graphData and graphDataColumns are fed into google visualizations
        [graphData: graphData, graphDataColumns: graphDataColumns, graphInstance: graphInstance]
    }

    /**
     * Basic demonstration of a function graph.
     * @return
     */
    def graph() {
        def graphData = [] //data points to be used in drawing the graph
        def expression = params.expression ?: "3 * sin(x) - 2 / (x - 2)" //a mathematical function to evaluate
        def graphDataColumns = [['number', 'x'], ['number', 'f(x)=' + expression]] //types and labels for axes

        //evaluate the function on the stored interval
        def lowerBound = DEFAULT_LOWER_BOUND
        def upperBound = DEFAULT_UPPER_BOUND

        for(def varX=lowerBound; varX<=upperBound; varX = varX + DEFAULT_STEP) {
            Calculable calc = new ExpressionBuilder(expression)
                    .withVariable("x", varX)
                    .build()

            def result = calc.calculate()

            graphData.add([varX, result]) //add the result to the collection of data points
        }

        //add the following to the model; graphData and graphDataColumns are fed into google visualizations
        [graphData: graphData, graphDataColumns: graphDataColumns, expression: expression]
    }
}
