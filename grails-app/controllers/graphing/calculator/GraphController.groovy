package graphing.calculator

import org.springframework.dao.DataIntegrityViolationException

class GraphController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [graphInstanceList: Graph.list(params), graphInstanceTotal: Graph.count()]
    }

    def create() {
        [graphInstance: new Graph(params)]
    }

    def save() {
        def graphInstance = new Graph(params)
        if (!graphInstance.save(flush: true)) {
            render(view: "create", model: [graphInstance: graphInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'graph.label', default: 'Graph'), graphInstance.id])
        redirect(action: "show", id: graphInstance.id)
    }

    def show(Long id) {
        def graphInstance = Graph.get(id)
        if (!graphInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "list")
            return
        }

        [graphInstance: graphInstance]
    }

    def edit(Long id) {
        def graphInstance = Graph.get(id)
        if (!graphInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "list")
            return
        }

        [graphInstance: graphInstance]
    }

    def update(Long id, Long version) {
        def graphInstance = Graph.get(id)
        if (!graphInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (graphInstance.version > version) {
                graphInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'graph.label', default: 'Graph')] as Object[],
                          "Another user has updated this Graph while you were editing")
                render(view: "edit", model: [graphInstance: graphInstance])
                return
            }
        }

        graphInstance.properties = params

        if (!graphInstance.save(flush: true)) {
            render(view: "edit", model: [graphInstance: graphInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'graph.label', default: 'Graph'), graphInstance.id])
        redirect(action: "show", id: graphInstance.id)
    }

    def delete(Long id) {
        def graphInstance = Graph.get(id)
        if (!graphInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "list")
            return
        }

        try {
            graphInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'graph.label', default: 'Graph'), id])
            redirect(action: "show", id: id)
        }
    }
}
