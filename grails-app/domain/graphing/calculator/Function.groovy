package graphing.calculator

class Function {
    String expression
    BigDecimal xValue
    BigDecimal yValue
    BigDecimal xLowerBound
    BigDecimal xUpperBound

    static constraints = {
        xLowerBound nullable: true
        xUpperBound nullable: true
        xValue nullable: true
        yValue nullable: true
    }
}
