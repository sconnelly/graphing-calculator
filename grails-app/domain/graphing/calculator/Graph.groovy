package graphing.calculator

class Graph {
    BigDecimal verticalLowerBound
    BigDecimal verticalUpperBound
    BigDecimal horizontalLowerBound
    BigDecimal horizontalUpperBound
    String title
    String width
    String height

    static hasMany = [functions: Function]

    static constraints = {
        verticalLowerBound nullable: true
        verticalUpperBound nullable: true
        horizontalLowerBound nullable: true
        horizontalUpperBound nullable: true
        width nullable: true
        height nullable: true
    }
}
