
<%@ page import="graphing.calculator.Graph" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'graph.label', default: 'Graph')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-graph" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-graph" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list graph">
			
				<g:if test="${graphInstance?.verticalLowerBound}">
				<li class="fieldcontain">
					<span id="verticalLowerBound-label" class="property-label"><g:message code="graph.verticalLowerBound.label" default="Vertical Lower Bound" /></span>
					
						<span class="property-value" aria-labelledby="verticalLowerBound-label"><g:fieldValue bean="${graphInstance}" field="verticalLowerBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.verticalUpperBound}">
				<li class="fieldcontain">
					<span id="verticalUpperBound-label" class="property-label"><g:message code="graph.verticalUpperBound.label" default="Vertical Upper Bound" /></span>
					
						<span class="property-value" aria-labelledby="verticalUpperBound-label"><g:fieldValue bean="${graphInstance}" field="verticalUpperBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.horizontalLowerBound}">
				<li class="fieldcontain">
					<span id="horizontalLowerBound-label" class="property-label"><g:message code="graph.horizontalLowerBound.label" default="Horizontal Lower Bound" /></span>
					
						<span class="property-value" aria-labelledby="horizontalLowerBound-label"><g:fieldValue bean="${graphInstance}" field="horizontalLowerBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.horizontalUpperBound}">
				<li class="fieldcontain">
					<span id="horizontalUpperBound-label" class="property-label"><g:message code="graph.horizontalUpperBound.label" default="Horizontal Upper Bound" /></span>
					
						<span class="property-value" aria-labelledby="horizontalUpperBound-label"><g:fieldValue bean="${graphInstance}" field="horizontalUpperBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="graph.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${graphInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="graph.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${graphInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.functions}">
				<li class="fieldcontain">
					<span id="functions-label" class="property-label"><g:message code="graph.functions.label" default="Functions" /></span>
					
						<g:each in="${graphInstance.functions}" var="f">
						<span class="property-value" aria-labelledby="functions-label"><g:link controller="function" action="show" id="${f.id}">${f?.expression}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${graphInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="graph.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${graphInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${graphInstance?.id}" />
					<g:link class="edit" action="edit" id="${graphInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
