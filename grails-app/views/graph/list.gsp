
<%@ page import="graphing.calculator.Graph" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'graph.label', default: 'Graph')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-graph" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-graph" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
                        <g:sortableColumn property="title" title="${message(code: 'graph.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="verticalLowerBound" title="${message(code: 'graph.verticalLowerBound.label', default: 'Vertical Lower Bound')}" />
					
						<g:sortableColumn property="verticalUpperBound" title="${message(code: 'graph.verticalUpperBound.label', default: 'Vertical Upper Bound')}" />
					
						<g:sortableColumn property="horizontalLowerBound" title="${message(code: 'graph.horizontalLowerBound.label', default: 'Horizontal Lower Bound')}" />
					
						<g:sortableColumn property="horizontalUpperBound" title="${message(code: 'graph.horizontalUpperBound.label', default: 'Horizontal Upper Bound')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${graphInstanceList}" status="i" var="graphInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td><g:link action="show" id="${graphInstance.id}" title="${graphInstance?.functions?.first()?.expression}">${fieldValue(bean: graphInstance, field: "title")}</g:link></td>

						<td>${fieldValue(bean: graphInstance, field: "verticalLowerBound")}</td>
					
						<td>${fieldValue(bean: graphInstance, field: "verticalUpperBound")}</td>
					
						<td>${fieldValue(bean: graphInstance, field: "horizontalLowerBound")}</td>
					
						<td>${fieldValue(bean: graphInstance, field: "horizontalUpperBound")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${graphInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
