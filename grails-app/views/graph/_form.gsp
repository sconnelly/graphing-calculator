<%@ page import="graphing.calculator.Graph" %>



<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'verticalLowerBound', 'error')} ">
	<label for="verticalLowerBound">
		<g:message code="graph.verticalLowerBound.label" default="Vertical Lower Bound" />
		
	</label>
	<g:field name="verticalLowerBound" value="${fieldValue(bean: graphInstance, field: 'verticalLowerBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'verticalUpperBound', 'error')} ">
	<label for="verticalUpperBound">
		<g:message code="graph.verticalUpperBound.label" default="Vertical Upper Bound" />
		
	</label>
	<g:field name="verticalUpperBound" value="${fieldValue(bean: graphInstance, field: 'verticalUpperBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'horizontalLowerBound', 'error')} ">
	<label for="horizontalLowerBound">
		<g:message code="graph.horizontalLowerBound.label" default="Horizontal Lower Bound" />
		
	</label>
	<g:field name="horizontalLowerBound" value="${fieldValue(bean: graphInstance, field: 'horizontalLowerBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'horizontalUpperBound', 'error')} ">
	<label for="horizontalUpperBound">
		<g:message code="graph.horizontalUpperBound.label" default="Horizontal Upper Bound" />
		
	</label>
	<g:field name="horizontalUpperBound" value="${fieldValue(bean: graphInstance, field: 'horizontalUpperBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'width', 'error')} ">
	<label for="width">
		<g:message code="graph.width.label" default="Width" />
		
	</label>
	<g:textField name="width" value="${graphInstance?.width}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="graph.height.label" default="Height" />
		
	</label>
	<g:textField name="height" value="${graphInstance?.height}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'functions', 'error')} ">
	<label for="functions">
		<g:message code="graph.functions.label" default="Functions" />
		
	</label>
	<g:select name="functions"
              from="${graphing.calculator.Function.list()}"
              multiple="multiple"
              optionKey="id"
              size="5"
              value="${graphInstance?.functions*.id}"
              class="many-to-many"
              optionValue="${{it.expression}}"
    />
</div>

<div class="fieldcontain ${hasErrors(bean: graphInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="graph.title.label" default="Title" />
		
	</label>
	<g:textField name="title" value="${graphInstance?.title}"/>
</div>

