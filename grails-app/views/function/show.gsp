
<%@ page import="graphing.calculator.Function" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'function.label', default: 'Function')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-function" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-function" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list function">
			
				<g:if test="${functionInstance?.xLowerBound}">
				<li class="fieldcontain">
					<span id="xLowerBound-label" class="property-label"><g:message code="function.xLowerBound.label" default="XL ower Bound" /></span>
					
						<span class="property-value" aria-labelledby="xLowerBound-label"><g:fieldValue bean="${functionInstance}" field="xLowerBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${functionInstance?.xUpperBound}">
				<li class="fieldcontain">
					<span id="xUpperBound-label" class="property-label"><g:message code="function.xUpperBound.label" default="XU pper Bound" /></span>
					
						<span class="property-value" aria-labelledby="xUpperBound-label"><g:fieldValue bean="${functionInstance}" field="xUpperBound"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${functionInstance?.xValue}">
				<li class="fieldcontain">
					<span id="xValue-label" class="property-label"><g:message code="function.xValue.label" default="XV alue" /></span>
					
						<span class="property-value" aria-labelledby="xValue-label"><g:fieldValue bean="${functionInstance}" field="xValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${functionInstance?.yValue}">
				<li class="fieldcontain">
					<span id="yValue-label" class="property-label"><g:message code="function.yValue.label" default="YV alue" /></span>
					
						<span class="property-value" aria-labelledby="yValue-label"><g:fieldValue bean="${functionInstance}" field="yValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${functionInstance?.expression}">
				<li class="fieldcontain">
					<span id="expression-label" class="property-label"><g:message code="function.expression.label" default="Expression" /></span>
					
						<span class="property-value" aria-labelledby="expression-label"><g:fieldValue bean="${functionInstance}" field="expression"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${functionInstance?.id}" />
					<g:link class="edit" action="edit" id="${functionInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
