
<%@ page import="graphing.calculator.Function" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'function.label', default: 'Function')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-function" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-function" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
                        <g:sortableColumn property="expression" title="${message(code: 'function.expression.label', default: 'Expression')}" />
					
						<g:sortableColumn property="xLowerBound" title="${message(code: 'function.xLowerBound.label', default: 'X Lower Bound')}" />
					
						<g:sortableColumn property="xUpperBound" title="${message(code: 'function.xUpperBound.label', default: 'X Upper Bound')}" />
					
						<g:sortableColumn property="xValue" title="${message(code: 'function.xValue.label', default: 'X Value')}" />
					
						<g:sortableColumn property="yValue" title="${message(code: 'function.yValue.label', default: 'Y Value')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${functionInstanceList}" status="i" var="functionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td><g:link action="show" id="${functionInstance.id}">${fieldValue(bean: functionInstance, field: "expression")}</g:link></td>

						<td>${fieldValue(bean: functionInstance, field: "xLowerBound")}</td>
					
						<td>${fieldValue(bean: functionInstance, field: "xUpperBound")}</td>
					
						<td>${fieldValue(bean: functionInstance, field: "xValue")}</td>
					
						<td>${fieldValue(bean: functionInstance, field: "yValue")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${functionInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
