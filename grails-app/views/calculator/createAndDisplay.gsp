<%--
  Created by IntelliJ IDEA.
  User: sconnelly
  Date: 12/18/12
--%>

<%@ page import="graphing.calculator.Graph" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Graphing Calculator</title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" controller="graph" action="list">Stored Graphs</g:link></li>
        <li><g:link class="list" controller="function" action="list">Stored Functions</g:link></li>
    </ul>
</div>
<div>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="createAndDisplay" method="get">
        <fieldset class="form">
            <div class="fieldcontain">
                <label for="selectedGraphId">
                    <g:message code="function.graph.id.label" default="Load Existing Graph" />

                </label>
                <g:select name="selectedGraphId"
                          from="${Graph.list()}"
                          optionValue="${{it.functions?.first()?.expression + '' + (it.functions?.size() > 1 ? '...' : '')}}"
                          value="${params?.id ?: ''}"
                          noSelection="['':'---Choose Existing---']"
                          optionKey="id" onchange="this.form.submit()"/>
            </div>
        </fieldset>
    </g:form>
</div>

<div>
    <g:form action="save" >
        <fieldset class="form">
            <div style="width: 300px; float: left;">
                <g:render template="form"/>
            </div>
            <div id="chart-container" style="position: relative; float: right;">
                <g:if test="${params.id}">
                    <g:if test="${Graph.get(params.id)}">
                        <g:include action="graphStoredData" params="[id: params.id]"/>
                    </g:if>
                    <g:else>
                        No Graph available for ID: ${params.id}
                    </g:else>
                </g:if>
            </div>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Save')}" />
        </fieldset>
    </g:form>
</div>

</body>
</html>