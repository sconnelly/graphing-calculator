<%@ page import="graphing.calculator.Function" %>



<div class="fieldcontain ${hasErrors(bean: functionInstance, field: 'xLowerBound', 'error')} ">
	<label for="xLowerBound">
		<g:message code="function.xLowerBound.label" default="X Lower Bound" />
		
	</label>
	<g:field name="xLowerBound" value="${fieldValue(bean: functionInstance, field: 'xLowerBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: functionInstance, field: 'xUpperBound', 'error')} ">
	<label for="xUpperBound">
		<g:message code="function.xUpperBound.label" default="X Upper Bound" />
		
	</label>
	<g:field name="xUpperBound" value="${fieldValue(bean: functionInstance, field: 'xUpperBound')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: functionInstance, field: 'xValue', 'error')} ">
	<label for="xValue">
		<g:message code="function.xValue.label" default="X Value" />
		
	</label>
	<g:field name="xValue" value="${fieldValue(bean: functionInstance, field: 'xValue')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: functionInstance, field: 'yValue', 'error')} ">
	<label for="yValue">
		<g:message code="function.yValue.label" default="Y Value" />
		
	</label>
	<g:field name="yValue" value="${fieldValue(bean: functionInstance, field: 'yValue')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: functionInstance, field: 'expression', 'error')} ">
	<label for="expression">
		<g:message code="function.expression.label" default="Expression" />
		
	</label>
	<g:textField name="expression" value="${functionInstance?.expression}"/>
</div>

