<%--
  Created by IntelliJ IDEA.
  User: sconnelly
  Date: 12/18/12
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<gvisualization:apiImport/>
<html>
<head>
    <title>Graphing Calculator</title>
</head>
<body>
<gvisualization:lineCoreChart hAxis="${new Expando([title: 'x'])}"
                              vAxis="${new Expando([title: 'y', viewWindowMode: 'explicit', viewWindow: new Expando([min: -5, max: 5])])}"
                              curveType="function"
                              elementId="linechart"
                              width="${400}"
                              height="${240}"
                              title="Function Graph"
                              columns="${graphDataColumns}"
                              data="${graphData}"/>
<div id="linechart"></div>
</body>
</html>