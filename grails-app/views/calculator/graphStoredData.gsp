<%--
  Created by IntelliJ IDEA.
  User: sconnelly
  Date: 12/18/12
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<gvisualization:apiImport/>
<html>
<head>
    <title>Graphing Calculator</title>
</head>
<body>
<gvisualization:lineCoreChart hAxis="${new Expando([title: 'x', viewWindowMode: 'explicit',
                                        viewWindow: new Expando([
                                            min: graphInstance?.horizontalLowerBound ?: -5,
                                            max: graphInstance?.horizontalUpperBound ?: 5])])}"
                              vAxis="${new Expando([title: 'y', viewWindowMode: 'explicit',
                                        viewWindow: new Expando([
                                            min: graphInstance?.verticalLowerBound ?: -5,
                                            max: graphInstance?.verticalUpperBound ?: 5])])}"
                              curveType="function"
                              elementId="linechart"
                              width="${graphInstance?.width}"
                              height="${graphInstance?.height}"
                              title="${graphInstance?.title}"
                              columns="${graphDataColumns}"
                              data="${graphData}" />
<div id="linechart"></div>
</body>
</html>